import glob
from PIL import Image
from statistics import mean
from statistics import median

images = glob.glob('C:\\git\\tensorFlow\\workspace\\training_demo\\images\\*\\*.jpg')
image_sizes = []
for image in images:
    im = Image.open(image)
    width, height = im.size
    image_sizes.append((width, height, image))
print("sorted width by width")
print("\n".join(map(str, sorted(image_sizes, key=lambda x: x[0]))))
print("sorted width by height")
print("\n".join(map(str, sorted(image_sizes, key=lambda x: x[1]))))
print("max width =%d" % (max(map(lambda x: x[0], image_sizes))))
print("max height =%d" % (max(map(lambda x: x[1], image_sizes))))
print("min width =%d" % (min(map(lambda x: x[0], image_sizes))))
print("min height =%d" % (min(map(lambda x: x[1], image_sizes))))
print("avg width =%d" % (mean(map(lambda x: x[0], image_sizes))))
print("avg height =%d" % (mean(map(lambda x: x[1], image_sizes))))
print("median width =%d" % (median(map(lambda x: x[0], image_sizes))))
print("median height =%d" % (median(map(lambda x: x[1], image_sizes))))
