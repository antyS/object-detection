Zadanie polega na przetrenowaniu, uruchomieniu i ocenie wydajności modelu sieci neuronowych 
Faster R-CCN. Model ma wykrywać i klasyfikować obiekty do jednej klasy.

Proponowana implementacja modelu Faster R-CNN znajduje się w ramach "aplikacji" Object Detection rozwijanej w repozytorium Tensorflow https://github.com/tensorflow/models/tree/master/research/object_detection

Zadanie sklada sie z
1. Instalacja i konfiguracja aplikacji (w lolaknym srodowisku)
1a. Konfiguracja aplikacji do uzycia wstepnie przetrenowanego modelu Faster R-CNN. 
http://download.tensorflow.org/models/object_detection/faster_rcnn_inception_v2_coco_2018_01_28.tar.gz
Aplikacja umozliwia uzycie roznych modeli do detekcji i klasyfikacji obiektow na obrazach
2. Przygotowanie (zgodnie z instrukcja z aplikacji Ojbect Detection) zestawu treningowego i testowego w formacie tfrecord.
Ilość obrazków w powyższych "paczkach" nie jest zbyt duża. Zaleca się zastosowanie techniki image augmentation - można to zrobić jeszcze przed utworzeniem zestawów w formacie tfrecord lub odpowiednio konfigurujać proces treningu modelu.
3. Uruchomienie treningu
4. Uruchomienie przykładowej aplikacji (rekomenduje skorzystać z przykładowego notebooka znajdującego się w ramach aplikacji https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb
5. Interpretacja wyników

# 
![Losses.png](Losses.png)
![mAP.png](mAP.png)
![TensorBoardImageResult.png](TensorBoardImageResult.png)